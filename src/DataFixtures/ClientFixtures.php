<?php

namespace App\DataFixtures;

use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;


    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $client = $this->clientHandler->createClient([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass_1234',
        ]);

        $manager->persist($client);

        $client2 = $this->clientHandler->createClient([
            'email' => 'admin@mail.ru',
            'passport' => 'admin2018',
            'password' => 'admin'
        ]);

        $manager->persist($client2);
        $manager->flush();
    }
}
