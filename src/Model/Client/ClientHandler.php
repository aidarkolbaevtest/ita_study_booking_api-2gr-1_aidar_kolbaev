<?php
/**
 * Created by PhpStorm.
 * User: aidar
 * Date: 18.06.18
 * Time: 15:10
 */

namespace App\Model\Client;


use App\Entity\Client;

class ClientHandler
{
    public function createClient(array $data, bool $encode = true) {
        $client = new Client();
        $client->setUsername($data['email']);
        $client->setPassport($data['passport']);
        if ($encode) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }
        $client->setPassword($password);
        return $client;
    }

    public function encodePlainPassword(string $password):string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}