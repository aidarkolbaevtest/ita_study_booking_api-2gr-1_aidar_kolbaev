<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="ping")
     */
    public function pingAction()
    {
        return new JsonResponse('pong');
    }


    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("GET")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(string $passport, string $email, ClientRepository $clientRepository)
    {
        return new JsonResponse($clientRepository->findOneByPassportOrEmail($passport, $email) !== null);
    }


    /**
     * @Route("/check/{email}/{password}", name="app_client_check")
     * @Method("GET")
     * @param string $email
     * @param string $password
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientAction(string $email,string $password, ClientRepository $clientRepository) {
        $client = $clientRepository->findOneByPassportAndEmail($password, $email);
        if ($client) {
             return new JsonResponse($client->__toArray());
        } else {
            return new JsonResponse(false);
        }
    }


    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param ClientHandler $clientHandler
     * @return JsonResponse
     */
    public function registerAction(Request $request, ClientHandler $clientHandler)
    {
        $client = $clientHandler->createClient([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'passport' => $request->get('passport')
        ], false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();

        return new JsonResponse(true);
    }
}
