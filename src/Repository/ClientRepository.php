<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findOneByPassportOrEmail(string $passport, string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.email = :email')
                ->orWhere('c.passport = :passport')
                ->setParameter('email', $email)
                ->setParameter('passport', $passport)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findOneByPassportAndEmail(string $plainPassword, string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.email = :email')
                ->andWhere('c.password = :password')
                ->setParameter('email', $email)
                ->setParameter('password', $plainPassword)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
