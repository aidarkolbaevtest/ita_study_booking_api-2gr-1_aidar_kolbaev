<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 2/15/18
 * Time: 1:53 PM
 */

namespace App\Security\PreAuthenticatedToken;


use App\Entity\Client;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;

class AnonymousPreAuthenticatedToken extends PreAuthenticatedToken
{
    public function __construct(string $providerKey)
    {
        $user = new Client('ANONYMOUS', null, ['ROLE_ANONYMOUS']);
        parent::__construct($user, null, $providerKey, ['ROLE_ANONYMOUS']);
    }
}
